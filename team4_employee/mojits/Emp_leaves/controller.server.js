/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('emp_leaves', function(Y, NAME) {

/**
 * The emp_leaves module.
 *
 * @module emp_leaves
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            //ac.params.getFromBody('startdate');
            //ac.models.get('model
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
                ac.done({
                    empleaves : [
                        {
                            date : '01-01-2014',
                            status : 'absent'
                        },
                        {
                            date : '02-01-2014',
                            status : 'present'
                        }

                    ]
                },"index");
            });
        },
        checkleaves : function(ac){
            var startDate=ac.params.getFromUrl("startDate");
            var endDate=ac.params.getFromUrl("endDate");
            console.log(startDate);
            console.log(endDate);

            ac.models.get('model').checkLeaves(startDate,endDate,function(err,data){
                console.log('returned back to controller');
                if(err){
                       ac.error(err);
                       return;
                   }
                console.log(data);
                   ac.done({

                       data:data.leaves_details
                   }, "json");

               });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon','mojito-params-addon']});
