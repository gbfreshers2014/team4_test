/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('emp_leaves-binder-index', function(Y, NAME) {

/**
 * The emp_leaves-binder-index module.
 *
 * @module emp_leaves-binder-index
 */

    /**
     * Constructor for the Emp_leavesBinderIndex class.
     *
     * @class Emp_leavesBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            var dtdate = Y.DataType.Date;

            var calendar1 = new Y.Calendar({
                contentBox: "#start_date_calendar",
                width:'200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date()}).render();

            calendar1.on("selectionChange", function (ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                Y.one("#start_date_text").set('value',dtdate.format(newDate));
            });

            // Get a reference to Y.DataType.Date

            var calendar2 = new Y.Calendar({
                contentBox: "#end_date_calendar",
                width:'200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date()}).render();



            calendar2.on("selectionChange", function (ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                Y.one("#end_date_text").set('value',dtdate.format(newDate));
            });

            var butt=Y.one("#submit_date");
            console.log('i am above button function');
                butt.on("click",function(e) {
                    e.preventDefault();
                    var self = this;
                    console.log('i am here');
                    var text1 = Y.one("#start_date_text").get('value');

                    var text2 = Y.one("#end_date_text").get('value');

                    var text3="1234";
                    Y.io("/v1/emp-portal/uuid/checkleaves.json", {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/JSON"
                        },
                        data: "startDate="+text1+"&endDate="+text2,

                        on: {
                            complete: function (id, o, args) {

                                alert('this executed');

                                YUI().use("datatable", function (Y) {
                                    var response = o.responseText;

                                    alert(response);
                                    var simple = new Y.DataTable({
                                        columns: ["date", "status"],
                                        data: JSON.parse(response).data
                                    });
                                    simple.render("#simple");
                                    //this.mojitProxy.invoke('index',{params:{start_date:text1,end_date:text2}},function(data){

                                });

//                                var response = o.responseText;
//
//                                alert(response);
//                                if(response === "V")
//                                {
//
//                                    Y.config.win.location = 'http://192.168.1.101:8666/admin';
//                                }
//                                else if(response.msg == "error")
//                                {
//                                    alert('failure');
//                                }
                            }
                        }
                    });

                });
            /**
             * Example code for the bind method:
             *
             * node.all('dt').on('mouseenter', function(evt) {
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).addClass('sel');
             *
             * });
             * node.all('dt').on('mouseleave', function(evt) {
             *   
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).removeClass('sel');
             *
             * });
             */
        }

    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client','calendar','event-mouseclick']});
