/*jslint anon:true, sloppy:true, nomen:true*/

YUI.add('emp_leaves-model', function(Y, NAME) {

/**
 * The emp_leaves-model module.
 *
 * @module emp_leaves
 */

    /**
     * Constructor for the Emp_leavesModel class.
     *
     * @class Emp_leavesModel
     * @constructor
     */
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        checkLeaves : function(startDate,endDate,callback){

            request({
                url: " http://localhost:8080/employee_management_restapi-1.0.0-SNAPSHOT/employee-portal/employees/1235/leaves.json?start_date="+startDate+"&end_date="+endDate,
                //headers: {"Content-Type": "application/json"},
                method: "GET",
                json: true

            }, function(err,res, body){

                callback(null, body);

            });

        }

    };

}, '0.0.1', {requires: ['mojit-params-addon']});
