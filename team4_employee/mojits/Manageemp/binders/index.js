/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('manageemp-binder-index', function(Y, NAME) {

/**
 * The manageemp-binder-index module.
 *
 * @module manageemp-binder-index
 */

    /**
     * Constructor for the ManageempBinderIndex class.
     *
     * @class ManageempBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.pagination(node);
        },
        pagination: function(node){
            alert('inside pagination');

            Y.all('td', 'table').each(function(i) {
                $(this).text(i+1);
            });



            Y.all('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = 10;
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertBefore($table).find('span.page-number:first').addClass('active');
            });
        }
    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client']});
