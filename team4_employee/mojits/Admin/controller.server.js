
YUI.add('admin', function(Y, NAME) {


    Y.namespace('mojito.controllers')[NAME] = {
        
	index: function(ac) {
                if(ac._adapter.req.user.role!== "admin")
                {
                   // ac.redirectURL('/');   insert proper redirect command here
                }
				ac.models.get('model').getData(function(err, data) {
                	if (err) {
                    		ac.error(err);
                    		return;
                		}
                	ac.assets.addCss('./index.css');
                	ac.done({
                            PanelName: 'Admin Panel',
                            RoleName: 'Admin',
                            admin: 'true',
                            class1: 'selected',
                    		status: 'Admin Page is working.',
                    		data: data
                		});
             		});

	}
        /*,
	authenticate: function(ac){
            ac.models.get('model').validation(ac,function(err,res){

            ac.done({uuid:res.uuid},'json');


        });



	}*/

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-session-addon', 'mojito-params-addon','admin-model', 'mojito-http-addon','mojito-models-addon']});
