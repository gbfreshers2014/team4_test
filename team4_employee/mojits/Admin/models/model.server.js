/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('admin-model', function(Y, NAME) {

/**
 * The admin-model module.
 *
 * @module admin
 */

    /**
     * Constructor for the AdminModel class.
     *
     * @class AdminModel
     * @constructor
     */
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        }
        /*,

        validation: function(ac,callback){

// Gwynniebee REST API connection TEST, result: successful
/*
            var url="http://api-dev.gwynniebee.com:4080/v1/recommendation/garments/ns/0/skus.json";
            var requestParams = {
                user_uuid: "c35aad41-9ae2-3ac7-ac0f-16067a86e27c",
                limit: 15
            };
            var requestHeaders = {
                contentType: "application/json; charset=utf-8",
                timeout: 12000
            };
            Y.mojito.lib.REST.GET(url,requestParams, requestHeaders, function(err,response) {
                if (err) {
                    callback(null, {key: "error"});
                    console.log(err);
                    //    return callback(err);
                }
                else {
                    console.log(response);
                }
            });
*/



//Self tried connection of REST API, result: failed
/*
            var url="http://192.168.1.112:8080/employee_management_restapi-1.0.0-SNAPSHOT/login/user_credentials.json";
            var requestHeaders = {
                    contentType: "application/json; charset=utf-8",
                    timeout:12000
                },
                requestParams = {
                    emailid: ac.params.getFromBody('username'),
                    password: ac.params.getFromBody('password')
                }
                ;
           // console.log(JSON.stringify({emailid:"admin@admin.com",password:"1234"}));
            Y.mojito.lib.REST.POST(url,JSON.stringify({
                emailid:"admin@admin.com",
                password:"1234"
            }), requestHeaders, function(err,response) {
                if (err) {
                    callback(null, {key: "error"});
                    console.log(err);
                    //    return callback(err);
                }
                else {
                    console.log(response);
                }
                    callback(null, {key : "success"});
            });



            request({
                url: " http://localhost:8080/employee_management_restapi-1.0.0-SNAPSHOT/login/user_credentials.json",
                //headers: {"Content-Type": "application/json"},
                method: "POST",
                body: {
                    "emailid" :ac.params.getFromBody('username'),
                    "password" :ac.params.getFromBody('password')
                },
                json: true

            }, function(err,res, body){

                console.log(err || body);
                console.log(typeof body);

                callback(null,{uuid: body.UUID});

            });


        }
*/
    };

}, '0.0.1', {requires: ['mojito-rest-lib','mojito-params-addon']});
