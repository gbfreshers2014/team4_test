YUI.add('addemp-model', function(Y, NAME) {
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },


        getData: function(callback) {

            callback(null, { some: 'data' });
        },
        pushData: function(ac,callback){
            request({

                url: "http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employees.json",
                headers: {"Content-Type": "application/json"},
                method: "POST",
                body:  JSON.stringify(ac.params.getFromBody('jsonObj'))


            }, function(err,res, body){
                console.log("error msg:"+err);
                console.log("body content:"+body);
                callback(null,body);
            });

        }


    };

}, '0.0.1', {requires: ['mojito-params-addon','mojito-client']});
