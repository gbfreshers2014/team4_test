YUI.add('addemp', function(Y, NAME) {
    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
                ac.done({
                    PanelName: 'Admin Panel',
                    RoleName: 'Admin',
                    class1: 'selected',
                    status: 'Mojito is working.',
                    updatinguuid: ac._adapter.req.user.uuid,
                    data: data
                });
            });
        },
        addfn:function(ac)
        {
            console.log('from controler: '+ac.params.getFromBody('jsonObj'));

            ac.models.get('model').pushData(ac,function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');

                console.log('from controler: '+ac.params.getFromBody('jsonObj'));

                ac.done({
                    message:data.message
                }, "index");
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});
