/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('attendancetasks-model', function(Y, NAME) {

/**
 * The attendancetasks-model module.
 *
 * @module attendancetasks
 */

    /**
     * Constructor for the AttendanceTasksModel class.
     *
     * @class AttendanceTasksModel
     * @constructor
     */

    var request = require('request');
    var fs = require('fs');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        uploadAttendance: function(date, attaction, filePath, callback) {
            /*request({
                url: "http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employee-portal/leaves.json",
                headers: {"Content-Type": "multipart/form-data"},
                method: "POST",
                body: reqObj,
                json: true

            }, function(err,res, body){
                console.log(body);
                callback(null, body);
            });*/
            var r = request.post("http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employee-portal/leaves.json", function (err, res, body) {
                if(err) {
                    callback(err);
                    return;
                }
                console.log('model-upload');
                if(filePath) {
                    fs.unlink(filePath, function (err) {
                        if(err) {
                            console.log(err);
                            return;
                        }
                        console.log('file deleted');
                    });
                }
                console.log(body);
                callback(null, body);
            })
            var form = r.form();
            form.append('date', date);
            form.append('action', attaction);
            if(attaction === 'uploadcsv')
                form.append('file', fs.createReadStream(filePath));

        }

    };

}, '0.0.1', {requires: []});
