/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('attendancetasks', function(Y, NAME) {

/**
 * The attendancetasks module.
 *
 * @module attendancetasks
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    var fs = require('fs');
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */

        index: function(ac) {
            ac.assets.addCss('./index.css');
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.done({});
            });
        },

        dateAttendance: function(ac) {
            //console.log("date" + ac.params.getFromBody('date'));
            ac.done({date : ac.params.getFromBody('date')}, "upload");
        },

        upload: function(ac) {
            console.log('upload function working');
            var req = ac.http.getRequest();
            var files = req.files;

            //console.log(req);

            var action = ac.params.getFromBody('attendanceAction');
            var date = ac.params.getFromBody('date');
            console.log(date + ' ' + action);
            var file = files && files.csvFile;
            //console.log(file);

            if(!date || !action || (action != 'allpresent' && action != 'holiday' && action != 'uploadcsv')) {
                ac.done({mssg: 'Malformed request 1'}, "uploadStatus");
                return;
            }

            if(action === 'uploadcsv') {
                if(!file) {
                  ac.done({mssg: 'Malformed request 2'}, "uploadStatus");
                  return;
                }
            }
            else {
              if(file) {
                fs.unlink(file.path, function(err) {
                  if(!err)
                    console.log('Temp file deleted successfully');
                  else
                    console.log('Error in deleting temp file');
                });
              }
            }

            ac.models.get('model').uploadAttendance(date, action, (file && file.path), function(err, res) {
                if(err)
                    ac.done({mssg: 'Internal Server Error'}, "uploadStatus");
                else {
                    if(!res.status || !res.status.code || res.status.code != 0)
                        ac.done({mssg : 'Upload Unsuccessful'}, "uploadStatus");
                    else
                        ac.done({mssg : 'Upload Successful'}, "uploadStatus");
                }
            });
            //console.log(files.csvFile);
            /*fs = require('fs')
            fs.readFile(files.csvFile.path, 'utf8', function (err,data) {
              if (err) {
                return console.log(err);
              }
              console.log(data);
              ac.models.get('model').uploadAttendance(data, function(err, res) {
                console.log(res);
                ac.done();
              });
            });*/

            //ac.http.redirect('/admin/attendancetasks', 302);
            //ac.redirect();
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon', 'mojito-http-addon', 'fs']});
