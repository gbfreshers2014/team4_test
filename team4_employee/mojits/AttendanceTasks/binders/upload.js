/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('attendancetasks-binder-upload', function(Y, NAME) {

/**
 * The attendancetasks-binder-index module.
 *
 * @module attendancetasks-binder-index
 */

    /**
     * Constructor for the AttendanceTasksBinderIndex class.
     *
     * @class AttendanceTasksBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;

            var file = node.one('#fileupload');
            var form = node.one('#uploadform');
            //console.log(file);
            //console.log('hello');
            var inp = node.all('input[name=attendanceAction]');
            inp.on("change", function(e) {
                if(e.target.get('value') === 'uploadcsv') {
                    file._node.required = true;
                    form.set('enctype','multipart/form-data');
                }
                else {
                    file._node.required = false;
                    form.set('enctype','application/x-www-form-urlencoded');
                }
            });
            
            /*var uploadButton = Y.one('#uploadButton');
            var form = Y.one('#content_box');
            console.log(form);
            uploadButton.on("click", btnClick);


            function btnClick(e) {
              var input = form.one('input[name=attendanceAction]:checked').get('value');
              console.log(input);

              if(input === 'uploadcsv') {
                console.log(form.one('#csvFile').get('value'));
                console.log(form.one('#csvFile').file);
                if(form.one('#csvFile').get(0).file[0] == null)
                  alert('Please upload a file or choose another option');
              }
            } */
            /**
             * Example code for the bind method:
             *
             * node.all('dt').on('mouseenter', function(evt) {
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).addClass('sel');
             *
             * });
             * node.all('dt').on('mouseleave', function(evt) {
             *   
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).removeClass('sel');
             *
             * });
             */
        }

    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'calendar', 'attribute']});
