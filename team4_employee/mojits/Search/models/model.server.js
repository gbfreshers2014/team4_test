YUI.add('search-model', function(Y, NAME) {
    var request = require('request');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        searchResult: function(ac,callback){

            request({
                url: "http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employee-portal/employee_search.json?param="+ac.params.getFromBody('param'),
                //headers: {"Content-Type": "application/json"},
                method: "GET",
                body: {

                },
                json: true

            }, function(err,res, body){
                callback(null,{emplist: body.list});
            });

        }

    };

}, '0.0.1', {requires: ['mojito-rest-lib','mojito-params-addon']});
