YUI.add('search', function(Y, NAME) {

    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
                ac.done({
                    status: 'Mojito is working.',
                    data: data
                });
            });
        },
        result: function(ac) {

            ac.models.get('model').searchResult(ac,function(err,res) {

                var employee_list = [];

                //if admin
                if(ac._adapter.req.user.role=="admin") {

                    for (var i = 0; i < res.emplist.length; i++) {

                        res.emplist[i].personalDetails.sno = i + 1;
                        res.emplist[i].personalDetails.admin = true;
                        employee_list.push(res.emplist[i].personalDetails);
                    }
                }
                else
                {
                    for(var i=0; i<res.emplist.length; i++)
                    {

                        res.emplist[i].sno = i+1;
                        res.emplist[i].admin = false;
                        employee_list.push(res.emplist[i].personalDetails);
                    }
                }
                if(ac._adapter.req.user.role=="admin") {
                    ac.done({emplist:employee_list, admin: true,RoleName: ac._adapter.req.user.role}, "index");
                }
                else
                  ac.done({emplist:employee_list,RoleName: ac._adapter.req.user.role}, "index");

             });
        }


    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});
