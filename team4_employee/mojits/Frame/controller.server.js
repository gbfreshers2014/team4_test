/**
 * Copyright 2013 Gwynnie Bee Inc.
 */

/*global YUI*/
YUI.add('FrameMojit', function (Y, NAME) {

    'use strict';

    Y.namespace('mojito.controllers')[NAME] = {

        index: function (ac) {
            this.__call(ac);
        },

        __call: function (ac) {
            var config = ac.config.get();
            var children = config.children;
            for (var key in children) {
                if (children.hasOwnProperty(key)) {
                    children[key].action = children[key].action || ac.action;
                }
            }

            ac.composite.execute(config, function (data, meta) {

                // meta.assets from child should be piped into
                // the frame's assets before doing anything else.
                ac.assets.addAssets(meta.assets);
                ac.assets.addCss('./index.css');

                if (ac.config.get('deploy') === true) {
                    ac.deploy.constructMojitoClientRuntime(ac.assets,
                        meta.binders);
                }
                if(ac._adapter && ac._adapter.req && ac._adapter.req.user && ac._adapter.req.user.role)
                {
                    if(ac._adapter.req.user.role=="admin") {
                    console.log('inside frame admin');
                    ac.done(
                        Y.merge(data, ac.assets.renderLocations(), {
                            title: ac.config.get('title'),
                            admin: 'true'
                        }),
                        Y.merge(meta, {
                            http: {
                                headers: {'content-type': 'text/html; charset="utf-8"'}
                            },
                            view: {name: 'index'}
                        })
                    );
                }
                else if(ac._adapter.req.user.role=="employee") {
                    console.log('inside frame not admin');
                    ac.done(
                        Y.merge(data, ac.assets.renderLocations(), {
                            title: ac.config.get('title'),
                            admin: 'false'
                        }),
                        Y.merge(meta, {
                            http: {
                                headers: {'content-type': 'text/html; charset="utf-8"'}
                            },
                            view: {name: 'index'}
                        })
                    );
                }}
                else{
                    ac.done(
                        Y.merge(data, ac.assets.renderLocations(), {
                            title: ac.config.get('title')
                        }),
                        Y.merge(meta, {
                            http: {
                                headers: {'content-type': 'text/html; charset="utf-8"'}
                            },
                            view: {name: 'index'}
                        })
                    );

                }


            });
        }
    };

}, '0.1.0', {requires: [
    'mojito',
    'mojito-assets-addon',
    'mojito-deploy-addon',
    'mojito-config-addon',
    'mojito-composite-addon'
]});
