YUI.add('login', function(Y, NAME) {
    Y.namespace('mojito.controllers')[NAME] = {

        index: function(ac) {
			
               	ac.assets.addCss('./index.css');
                	
                ac.done({},"login"); 
        }
    };
}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-data-addon', 'mojito-models-addon', "mojito-params-addon","mojito-http-addon"]});	
