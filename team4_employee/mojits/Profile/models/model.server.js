YUI.add('profile-model', function(Y, NAME) {
    var request = require('request');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        editData: function(uuidlink,callback) {
            request({
                url: "http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employees/"+uuidlink+".json",

                method: "GET",
                body: {
                },
                json: true

            }, function(err,res, body){
                console.log(body);
                console.log(body.employeeDetails.personalDetails.firstName);
                callback(null,{emplist: body.employeeDetails});
            });

        },
        profileData: function(uuidlink, callback){
            //console.log(ac.params.route('uuid'));

            request({
                url: "http://induction-dev.gwynniebee.com:8080/employee_REST_API-v1/employees/"+uuidlink+".json",

                method: "GET",
                body: {
                },
                json: true

            }, function(err,res, body){
                console.log(body);
                console.log(body.employeeDetails.personalDetails.firstName);
                callback(null,{emplist: body.employeeDetails});
            });

        }

    };

}, '0.0.1', {requires: ['mojito-rest-lib','mojito-params-addon']});
