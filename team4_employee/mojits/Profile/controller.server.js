YUI.add('profile', function(Y, NAME) {

    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
         /*       ac.done({
                    PanelName: 'Profile Page',
                    RoleName: 'Admin',
                    class2: 'selected',

                    status: 'Profile Page is working.',
                    data: data
                });*/
            });
            var uuidlink;
            if(ac.params.route('uuid'))
            {
                uuidlink=ac.params.route('uuid');
            }
            else
            {
                uuidlink=ac._adapter.req.user.uuid;

            }
            console.log(uuidlink);

            //to check whether admin is making changes or employee
            var check_role={};
            if(ac._adapter.req.user.role==="admin") {
                check_role.admin='true';
                check_role.main_role='admin';
            }
            else
            {
                check_role.admin=0;
                check_role.main_role='employee';

            }
            console.log('in js, checking check_role.admin'+check_role.admin);

            // /to check whether the employee is editing his own profile or trying to edit someone else profile
            if(uuidlink==ac._adapter.req.user.uuid)
                check_role.allow_access='true';
            else
                check_role.allow_access='false';

            ac.models.get('model').profileData(uuidlink,function(err, res) {

                console.log(res.emplist.personalDetails.firstName);
                var comm_list = {},
                    address_list=[],
                    family_details=[];
                for(var i=0;i<res.emplist.communicationDetails.length;i++)
                {
                    if (res.emplist.communicationDetails[i].type==='skype_id')
                    //comm_list.push({skypeId:res.emplist.communicationDetails[i].details});
                        comm_list.skypeId = res.emplist.communicationDetails[i].details;
                    if (res.emplist.communicationDetails[i].type==='phone_no')
                    //comm_list.push({phoneNo:res.emplist.communicationDetails[i].details});
                        comm_list.phoneNo=res.emplist.communicationDetails[i].details;
                    if (res.emplist.communicationDetails[i].type==='personal_email_id')
                    //comm_list.push({personalId:res.emplist.communicationDetails[i].details});
                        comm_list.personalId=res.emplist.communicationDetails[i].details;
                }
                for(i=0;i<res.emplist.addressDetails.length;i++)
                {
                    if (res.emplist.addressDetails[i].type==='permanent')

                        address_list.push({

                            perm_add:res.emplist.addressDetails[i].streetAddress,
                            perm_city:res.emplist.addressDetails[i].city,
                            perm_state:res.emplist.addressDetails[i].state,
                            perm_country:res.emplist.addressDetails[i].country,
                            perm_zipcode:res.emplist.addressDetails[i].Zipcode

                        });

                    if (res.emplist.addressDetails[i].type==='current')
                        address_list.push({

                            curr_add:res.emplist.addressDetails[i].streetAddress,
                            curr_city:res.emplist.addressDetails[i].city,
                            curr_state:res.emplist.addressDetails[i].state,
                            curr_country:res.emplist.addressDetails[i].country,
                            curr_zipcode:res.emplist.addressDetails[i].Zipcode

                        });
                }

                for (var i = 0; i < res.emplist.familyDetails.length; i++) {

                    family_details.push({
                        family_relation: res.emplist.familyDetails[i].relation,
                        family_name: res.emplist.familyDetails[i].fullName,
                        family_dependent: res.emplist.familyDetails[i].dependent,
                        family_dob: res.emplist.familyDetails[i].dob,
                        family_bloodGroup: res.emplist.familyDetails[i].bloodGroup
                    });
                }

                ac.done({
                    PanelName: 'Profile Page',
                    RoleName: ac._adapter.req.user.role,
                    class2: 'selected',
                    uuidlink: uuidlink,
                    admin:check_role.admin,

                    fname: res.emplist.personalDetails.firstName,
                    lname: res.emplist.personalDetails.lastName,
                    emailid: res.emplist.personalDetails.email,
                    empid: res.emplist.personalDetails.employeeId,
                    designation: res.emplist.personalDetails.designation,
                    bloodgrp:res.emplist.personalDetails.bloodGroup,
                    empstatus:res.emplist.personalDetails.employmentStatus,
                    dob:res.emplist.personalDetails.dob,
                    doj:res.emplist.personalDetails.doj,

                    skypeId:comm_list.skypeId,
                    phoneNo:comm_list.phoneNo,
                    personalId:comm_list.personalId,

                    addr:address_list,
                    family:family_details

                })


            });



        },
        edit:function(ac) {
            var uuidlink;
            if(ac.params.route('uuid'))
            {
                uuidlink=ac.params.route('uuid');
            }
            else
            {
                uuidlink=ac._adapter.req.user.uuid;

            }
            console.log(uuidlink);

            //to check whether admin is making changes or employee
            var check_role={};
            if(ac._adapter.req.user.role==="admin") {
                check_role.admin='true';
                check_role.main_role='admin';
            }
            else
            {
                check_role.admin=0;
                check_role.main_role='employee';

            }
            console.log('in js, checking check_role.admin'+check_role.admin);

            // /to check whether the employee is editing his own profile or trying to edit someone else profile
            if(uuidlink==ac._adapter.req.user.uuid)
                check_role.allow_access='true'
            else
                check_role.allow_access='false'

            ac.models.get('model').profileData(uuidlink,function(err, res) {

                console.log(res.emplist.personalDetails.firstName);
                var personal_details={},
                    comm_list = {},
                    address_list=[],
                    family_details=[];

                //personal_details.uuid: res.emplist.personalDetails.uuid;

                personal_details.fname= res.emplist.personalDetails.firstName;
                personal_details.lname= res.emplist.personalDetails.lastName;
                personal_details.emailid= res.emplist.personalDetails.email;
                personal_details.empid= res.emplist.personalDetails.employeeId;
                personal_details.bloodgrp=res.emplist.personalDetails.bloodGroup;
                personal_details.empstatus=res.emplist.personalDetails.employmentStatus;
                personal_details.dob=res.emplist.personalDetails.dob;
                personal_details.doj=res.emplist.personalDetails.doj;
                personal_details.designation= res.emplist.personalDetails.designation;


                for(var i=0;i<res.emplist.communicationDetails.length;i++)
                {
                    if (res.emplist.communicationDetails[i].type==='skype_id')
                    //comm_list.push({skypeId:res.emplist.communicationDetails[i].details});
                        comm_list.skypeId = res.emplist.communicationDetails[i].details;
                    if (res.emplist.communicationDetails[i].type==='phone_no')
                    //comm_list.push({phoneNo:res.emplist.communicationDetails[i].details});
                        comm_list.phoneNo=res.emplist.communicationDetails[i].details;
                    if (res.emplist.communicationDetails[i].type==='personal_email_id')
                    //comm_list.push({personalId:res.emplist.communicationDetails[i].details});
                        comm_list.personalId=res.emplist.communicationDetails[i].details;
                }
                for(i=0;i<res.emplist.addressDetails.length;i++)
                {
                    if (res.emplist.addressDetails[i].type==='permanent')

                        address_list.push({

                            perm_add:res.emplist.addressDetails[i].streetAddress,
                            perm_city:res.emplist.addressDetails[i].city,
                            perm_state:res.emplist.addressDetails[i].state,
                            perm_country:res.emplist.addressDetails[i].country,
                            perm_zipcode:res.emplist.addressDetails[i].Zipcode

                        });

                    if (res.emplist.addressDetails[i].type==='current')
                        address_list.push({

                            curr_add:res.emplist.addressDetails[i].streetAddress,
                            curr_city:res.emplist.addressDetails[i].city,
                            curr_state:res.emplist.addressDetails[i].state,
                            curr_country:res.emplist.addressDetails[i].country,
                            curr_zipcode:res.emplist.addressDetails[i].Zipcode

                        });
                }

                for (var i = 0; i < res.emplist.familyDetails.length; i++) {

                    family_details.push({
                        family_relation: res.emplist.familyDetails[i].relation,
                        family_name: res.emplist.familyDetails[i].fullName,
                        family_dependent: res.emplist.familyDetails[i].dependent,
                        family_dob: res.emplist.familyDetails[i].dob,
                        family_bloodGroup: res.emplist.familyDetails[i].bloodGroup
                    });
                }

                ac.done({
                    PanelName: 'Profile Edit Page',
                    RoleName: ac._adapter.req.user.role,
                //    class2: 'selected',
                    uuidlink: uuidlink,
                    admin:check_role.admin,
                    personl:personal_details,

                    comm:comm_list,
                    //   skypeId:comm_list.skypeId,
                    //   phoneNo:comm_list.phoneNo,
                    //   personalId:comm_list.personalId,

                    addr:address_list,
                    family:family_details

                },"edit")


            });
        },
        del:function(ac) {
            ac.models.get('model').deleteData(function(err, data) {

            ac.done({


                },"index")

            });

        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-params-addon', 'mojito-models-addon']});
