YUI.add('Profile-binder-edit', function(Y, NAME) {
    Y.namespace('mojito.binders')[NAME] = {
        init: function (mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        bind: function (node) {
            var me = this;
            this.node = node;
            this.bindButtons(node);
        },

        bindButtons: function (node) {
            alert("inside js now");
            Y.one('#submit_button').on("click", verification);
            function verification(e) {
                alert('inside verification');
                e.preventDefault();
                var err=0;
                alert(Y.one('#RoleCheck').get('value'));
                if (Y.one('#RoleCheck').get('value')==="employee")
                {
                    if (Y.one('#EmpCheck').get('value')=='false')
                    {
                        alert('You do not have Admin Rights');
                        Y.config.win.location="/profile";
                    }
                    else
                    {
                        Y.one("#firstName").setAttribute("readonly","readonly");
                        Y.one("#lastName").setAttribute("readonly","readonly");
                        Y.one("#emailId").setAttribute("readonly","readonly");
                        Y.one("#uuid").setAttribute("readonly","readonly");
                        Y.one("#dob").setAttribute("readonly","readonly");
                        Y.one("#doj").setAttribute("readonly","readonly");
                        Y.one("#empId").setAttribute("readonly","readonly");
                        Y.one("#designation").setAttribute("readonly","readonly");
                        Y.one("#phoneNo").setAttribute("readonly","readonly");
                        Y.one("#skypeId").setAttribute("readonly","readonly");
                        Y.one("#firstName").setAttribute("readonly","readonly");
                        Y.one("#firstName").setAttribute("readonly","readonly");
                        Y.one("#firstName").setAttribute("readonly","readonly");
                    }
                }

                if (Y.one('#bloodGroup').get('value').length < 2) {
                    err = 1;
                    Y.one('#bloodGroup').setStyle('background-color', "Red");
                }
                if (Y.one('#permAdd').get('value').length < 2) {
                    err = 1;
                    Y.one('#permAdd').setStyle('background-color', "Red");
                }
                if (Y.one('#permCity').get('value').length < 2) {
                    err = 1;
                    Y.one('#permCity').setStyle('background-color', "Red");
                }
                if (Y.one('#permState').get('value').length < 2) {
                    err = 1;
                    Y.one('#permState').setStyle('background-color', "Red");
                }
                if (Y.one('#permCountry').get('value').length < 2) {
                    err = 1;
                    Y.one('#permCountry').setStyle('background-color', "Red");
                }
                if (Y.one('#permZipcode').get('value').length < 2) {
                    err = 1;
                    Y.one('#permZipcode').setStyle('background-color', "Red");
                }
                if (Y.one('#currAdd').get('value').length < 2) {
                    err = 1;
                    Y.one('#currAdd').setStyle('background-color', "Red");
                }
                if (Y.one('#currCity').get('value').length < 2) {
                    err = 1;
                    Y.one('#currCity').setStyle('background-color', "Red");
                }
                if (Y.one('#currState').get('value').length < 2) {
                    err = 1;
                    Y.one('#currState').setStyle('background-color', "Red");
                }
                if (Y.one('#currCountry').get('value').length < 2) {
                    err = 1;
                    Y.one('#currCountry').setStyle('background-color', "Red");
                }
                if (Y.one('#currZipcode').get('value').length < 2) {
                    err = 1;
                    Y.one('#currZipcode').setStyle('background-color', "Red");
                }
/*                if (Y.one('#password').get('value').length < 2) {
                    err = 1;
                    Y.one('#password').setStyle('background-color', "Red");
                }
*/                if (err == 0) {
                    alert('all fields successfully entered');
                    var first_name = Y.one('#firstName').get('value'),
                        last_name = Y.one('#lastName').get('value'),
                        father_name = Y.one('#fatherName').get('value'),
                        mother_name = Y.one('#motherName').get('value'),
                        email_id = Y.one('#emailId').get('value'),
                        phone_no = Y.one('#phoneNo').get('value'),
                        skype_id = Y.one('#skypeId').get('value'),
                        emp_id = Y.one('#empId').get('value'),
                        uuid = Y.one('#uuid').get('value'),
  //                      pass=Y.one('#password').get('value'),
                        designation = Y.one('#designation').get('value'),
                        blood_group = Y.one('#bloodGroup').get('value'),
                        dob = Y.one('#dob').get('value'),
                        doj = Y.one('#doj').get('value'),
                        perm_add = Y.one('#permAdd').get('value'),
                        perm_city = Y.one('#permCity').get('value'),
                        perm_state = Y.one('#permState').get('value'),
                        perm_country = Y.one('#permCountry').get('value'),
                        perm_zipcode = Y.one('#permZipcode').get('value'),
                        curr_add = Y.one('#currAdd').get('value'),
                        curr_city = Y.one('#currCity').get('value'),
                        curr_state = Y.one('#currState').get('value'),
                        curr_country = Y.one('#currCountry').get('value'),
                        curr_zipcode = Y.one('#currZipcode').get('value');
           //         var role_value=Y.one('#role').get('value');
                    var emp_value=Y.one('#empStatus').get('value');

             //       var role= Y.one('#role').get('options').item(role_value).get('text');
                    var emp_status=Y.one('#empStatus').get('options').item(emp_value).get('text');


                    var jsonObj = {};
                    jsonObj.uuid=uuid;
                    jsonObj.employeeDetails = {};
                    jsonObj.employeeDetails.loginDetails = {
                        role: role,
                        password: pass
                    }
                    jsonObj.employeeDetails.personalDetails = {
                        firstName: first_name,
                        lastName: last_name,
                        email: email_id,
                        employeeId: emp_id,
                        designation: designation,
                        employmentStatus:emp_status,
                        bloodGroup: blood_group,
                        dob: dob,
                        doj: doj
                    };
                    jsonObj.employeeDetails.communicationDetails = [
                        {
                            type: 'skype_id',
                            details: skype_id
                        },
                        {
                            type: 'phone_no',
                            details: phone_no
                        }
                    ];
                    jsonObj.employeeDetails.familyDetails = [
                        {
                            relation: 'father',
                            fullName: father_name,
                            dependent: 'true',
                            bloodGroup: '',
                            dob: ''
                        },
                        {
                            relation: 'mother',
                            fullName: mother_name,
                            dependent: 'true',
                            bloodGroup: '',
                            dob: ''
                        }
                    ];
                    jsonObj.employeeDetails.addressDetails = [
                        {

                            type: 'permanent',
                            streetAddress: perm_add,
                            city: perm_city,
                            state: perm_state,
                            country: perm_country,
                            Zipcode: perm_zipcode
                        },
                        {

                            type: 'current',
                            streetAddress: curr_add,
                            city: curr_city,
                            state: curr_state,
                            country: curr_country,
                            Zipcode: curr_zipcode
                        }
                    ];
                    Y.io("/profile/:uuid/edit", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({jsonObj:jsonObj}),
                        on: {
                            complete: function (id, o, args) {
                                var response = o.responseText;

                                if (response.message === "updated_successfully") {
                                    alert('success');
                                    Y.config.win.location ='/admin';
                                }
                                else  {
                                    alert('failure');
                                }
                            }
                        }
                    });

                }

            }

        }

    };
}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click']});
