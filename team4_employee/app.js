'use strict';

var express = require('express'),
    libmojito = require('mojito'),
    cookieParser = require('cookie-parser'),
	  //bodyParser = require('body-parser'),
  	session = require('cookie-session'),
  	methodOverride = require('method-override'),
  	flash = require('connect-flash'),
  	passport = require('./authentication').passport,
    app;

app = express();
app.set('port', process.env.PORT || 8666);
libmojito.extend(app, {
    context: {
        environment: "development"
    }
});
app.use(libmojito.middleware());



app.use(cookieParser());
//app.use(bodyParser());
app.use(methodOverride());
app.use(session({ secret: 'gwn dog start' }));
app.use(express.json());
app.use(express.urlencoded({
  extended: true
})); 
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
//app.use(app.router);
//app.use(express.static(__dirname + '/../../public'));




app.mojito.attachRoutes();

app.get('/', libmojito.dispatch('loginframe.index'));
//via passport
app.post('/', passport.authenticate('local', { failureRedirect: '/', failureFlash: "Wrong username or password" }),
  	function(req, res) {
  		console.log(req.user);
    	//validation here
        if(req.user.role==='admin')
        {
            res.redirect('/admin');
        }
        else if(req.user.role==='employee')
          res.redirect('/profile/'+req.user.uuid);
        else
        {
            //error messages here

            res.end('Server error, please try again later');
        }

    });
//app.post('/', libmojito.dispatch('admin.authenticate'));
//app.post('/',l.ibmojito.dispatch('192.168.1.112:8080/EmpProjet-1.0.0-SNAPSHOT/authenticate.json'));
app.get('/admin',passport.ensureAuthenticated, libmojito.dispatch('adminframe.index'));
app.get('/profile', libmojito.dispatch('profileframe.index'));

app.get('/profile/:uuid/edit', libmojito.dispatch('profileframe.edit'));

app.get('/profile/:uuid', libmojito.dispatch('profileframe.index'));
app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('');
});
app.get('/leaves', libmojito.dispatch('leavesframe.index'));
app.get('/admin/add-emp', libmojito.dispatch('addempframe.index'));
app.post('/admin/add-emp', libmojito.dispatch('addemp.addfn'));
app.get('/admin/manage-emp', libmojito.dispatch('manageempframe.index'));
app.get('/admin/attendancetasks', libmojito.dispatch('attendancetasksframe.index'));
app.post('/admin/attendancetasks/dateAttendance', libmojito.dispatch('attendancetasksframe.dateAttendance'));
app.post('/admin/attendancetasks/upload', libmojito.dispatch('attendancetasksframe.upload'));
app.get('/search', libmojito.dispatch('searchframe.result'));
app.post('/search', libmojito.dispatch('searchframe.result'));
//app.get('/emp_profile',libmojito.dispatch('profile_emp_frame.index'));
app.get('/v1/emp-portal/uuid/leaves.json',libmojito.dispatch('empLeavesFrame.index'));
app.get('/v1/emp-portal/uuid/checkleaves.json',libmojito.dispatch('empLeaves.checkleaves'));


app.listen(app.get('port'), function () {
    console.log('Server listening on port ' + app.get('port') + ' ' +
                   'in ' + app.get('env') + ' mode');
});

 module.exports = app;
