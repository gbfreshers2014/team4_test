
var passport = require('passport'),
  http = require('http'),
  LocalStrategy = require('passport-local').Strategy;

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(id, done) {
  //findById(id, function (err, user) {
    done(null, id);
  //});
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    // asynchronous verification, for effect...
      console.log(username + " " + password);
    var jsonObj = JSON.stringify({
      "emailid" : username,
      "password" : password
    });
    var postheaders = {
      'Content-Type' : 'application/json',
      'Content-Length' : Buffer.byteLength(jsonObj, 'utf8')
  };
  var optionspost = {
      host : 'induction-dev.gwynniebee.com',
      port : 8080,
      path : '/employee_REST_API-v1/employee-portal/authenticate.json',
      method : 'POST',
      headers : postheaders
  };


    process.nextTick(function () {
      var resp_data = {};

      var reqPost = http.request(optionspost, function(res) {
              console.log("statusCode: ", res.statusCode);
              // uncomment it for header details
          //  console.log("headers: ", res.headers);
             res.on('error', function() {
                
              return done(null, null);
             });
              res.on('data', function(d) {
                  console.info('POST result:\n');
                  process.stdout.write(d);
                  resp_data = JSON.parse(d);
                  console.log(resp_data.status + ' gg');
                  console.info('\n\nPOST completed');
              //return done(null, resp_data);
              if(!resp_data.status)
                return done(null, null);
              if(resp_data.status.code == "0") {
                delete resp_data.status;
                return done(null, resp_data);
              }
              else {
                return done(null, null);
              }
                  
              });
          });
   
  // write the json data
  reqPost.write(jsonObj);
  reqPost.on('error', function(err) {
    console.log("error here");
    return done(err);
  })
  reqPost.end();

      // Find the user by username.  If there is no user with the given
      // username, or the password is not correct, set the user to `false` to
      // indicate failure and set a flash message.  Otherwise, return the
      // authenticated `user`.
      /*findByUsername(username, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
        if (user.password != password) { return done(null, false, { message: 'Invalid password' }); }
        return done(null, user);
      })*/
    });
  }
));

passport.ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/');
}

exports.passport = passport;
//exports.ensureAuthenticated = ensureAuthenticated;
